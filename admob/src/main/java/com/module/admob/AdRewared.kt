package com.module.admob

import android.app.Activity
import android.util.Log
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback

class AdRewared(private val activity: Activity, private var unitId: String) {
    private var rewardedAd: RewardedAd? = null
    private var rewardedAdListener: OnRewardedAdListener? = null

    fun getrewardedAd(): RewardedAd?{
        return this.rewardedAd
    }

    fun setrewardedAd(reward: RewardedAd?){
        this.rewardedAd = reward
    }

    fun loadRewardAd(context: Activity): RewardedAd?{
        RewardedAd.load(activity, unitId ,  AdRequest.Builder().build(), object :RewardedAdLoadCallback(){
            override fun onAdLoaded(rewardAd: RewardedAd) {
                Log.d("Load Reward","Complete")
                //rewardedAd = rewardAd
                setrewardedAd(rewardAd)
            }

            override fun onAdFailedToLoad(rewardError: LoadAdError) {
                Log.d("Load Reward",rewardError.message)
                //rewardedAd = null
                setrewardedAd(null)
            }
        })
        return getrewardedAd()//rewardedAd
    }

    fun showRewardAd(context: Activity){
        getrewardedAd()?.show(context, object : RewardedAdCallback(){
            override fun onUserEarnedReward(p0: RewardItem) {
                rewardedAdListener?.onUserEarnedReward()
            }

            override fun onRewardedAdClosed() {
                rewardedAdListener?.onRewardedAdClosed()
                loadRewardAd(context) //load reward
            }

            override fun onRewardedAdOpened() {
                rewardedAdListener?.onRewardedAdOpened()
            }
        })
    }

    fun setOnRewardedAdListener(rewardedListener: OnRewardedAdListener?){
        rewardedAdListener = rewardedListener
    }

    interface OnRewardedAdListener {
        fun onUserEarnedReward()
        fun onRewardedAdClosed()
        fun onRewardedAdOpened()
    }
}