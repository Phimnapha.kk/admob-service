package com.module.admob

import android.app.Activity
import android.content.Context
import android.view.ViewGroup
import androidx.core.view.size
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import java.util.*

class AdBanner(private var activity: Activity, private var adView: AdView)  {
    //private var unitId: String = "ca-app-pub-3940256099942544/6300978111"

    fun initialAdMob(admobId :String?) {
        val testDeviceIds = Arrays.asList("D0C42600A81B2EAF1DC180797B8BD69E")
        val requestConfiguration =
            RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build()
        MobileAds.setRequestConfiguration(requestConfiguration)
        MobileAds.initialize(activity, admobId)
    }

    fun loadBannerAd(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
       // holder.addView(adView)
    }
}