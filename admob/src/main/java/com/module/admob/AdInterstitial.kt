package com.module.admob

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.ViewGroup
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback

class AdInterstitial(private val activity: Activity, private val unitId: String) {
    private var mInterstitialAd: InterstitialAd? = null
    private val adRequest = AdRequest.Builder().build()
    private var TAG = "AdInterstitial"

    fun getInterstitialAd(): InterstitialAd?{
        if(mInterstitialAd == null)
            mInterstitialAd = loadInterstitialAd()
        return this.mInterstitialAd
    }

    fun loadInterstitialAd(): InterstitialAd? {
        InterstitialAd.load(activity, unitId, adRequest ,object: InterstitialAdLoadCallback() {
            override fun onAdLoaded(interstitialAd: InterstitialAd) {
                Log.d(TAG, "Load Complete");
                mInterstitialAd = interstitialAd
            }

            override fun onAdFailedToLoad(adError: LoadAdError) {
                Log.d(TAG, "${adError.message} \n${adError.domain} \n${adError.code}" );
                mInterstitialAd = null
            }
        })
        return mInterstitialAd
    }

    fun showInterstitialAd() {
        if(getInterstitialAd() == null) loadInterstitialAd()
        getInterstitialAd()?.show(activity) ?: Log.d(TAG, "The interstitial ad wasn't ready yet.")
        loadInterstitialAd()
    }

}