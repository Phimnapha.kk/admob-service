package com.module.admobservice

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.module.admob.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import kotlinx.android.synthetic.main.native_ad_layout.*
import kotlinx.android.synthetic.main.native_ad_layout.view.*
var rewardSuccess:Boolean = false
class MainActivity : AppCompatActivity() , AdRewared.OnRewardedAdListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //initial , load banner and show unitID init in xml
        var banner = AdBanner(this, adView_banner)
        //banner.initialAdMob("ca-app-pub-3940256099942544~3347511713")
        banner.initialAdMob(R.string.admob_id.toString())
        banner.loadBannerAd()

        //load interstitial
        var interstitial = AdInterstitial(this,"ca-app-pub-9972871614947844/3174870204")
        interstitial.getInterstitialAd()

        //show interstitial
        btn_ad_interstitial.setOnClickListener {
            Toast.makeText(this,"Click Interstitial :${interstitial.getInterstitialAd()}",Toast.LENGTH_SHORT).show()
            interstitial.showInterstitialAd()
        }

        //init & load
        var adRewarded = AdRewared(this, "ca-app-pub-9972871614947844/9505308987")
        adRewarded.loadRewardAd(this)

        //click reward
        btn_ad_reward.setOnClickListener {
            Toast.makeText(this, "Click reward ${adRewarded.getrewardedAd()}", Toast.LENGTH_SHORT).show()
            //adRewardedHandler.getInstance()?.initializeRewardedHandler(this);
            //adRewardedHandler.getInstance()?.showRewardedAd(this)

            //in activity
            /*var adRewardListener = object : RewardedAdCallback() {
                override fun onRewardedAdClosed() {

                }

                override fun onUserEarnedReward(p0: RewardItem) {
                    Log.d("Reward", "Get reward")
                }
            }
            adRewarded.loadRewardAd(this@MainActivity)
            adRewarded.getrewardedAd()?.show(this, adRewardListener)*/

            adRewarded.showRewardAd(this)
            adRewarded.setOnRewardedAdListener(this)
        }

        btn_ad_native.setOnClickListener {
            var intent = Intent(this,NativeActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onRewardedAdOpened(){
        Toast.makeText(this, "Video open.", Toast.LENGTH_SHORT).show()
        rewardSuccess = false
    }

    override fun onUserEarnedReward() {
        Toast.makeText(this, "Video end.", Toast.LENGTH_SHORT).show()
        rewardSuccess = true
    }

    override fun onRewardedAdClosed() {
        if(rewardSuccess)
            Toast.makeText(this, "Video close connect success", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(this, "Video not success", Toast.LENGTH_SHORT).show()

    }
}